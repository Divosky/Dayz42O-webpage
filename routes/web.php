<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('/', 'PagesController@index');
Route::get('/changelog', 'PagesController@changelog');
Route::get('/maps', 'PagesController@maps');
Route::get('/items', 'PagesController@items');
Route::get('/faq', 'PagesController@faq');
Route::get('/coins', 'PagesController@coins');
Route::get('/generator', 'PagesController@generator');
Route::get('/admin', 'PagesController@admin');
Route::get('/add-item', 'PagesController@additem');
Route::any('/admin/add-item', array('uses'=>'DbController@additem'));

Route::post('/lang', array(
  'as' => 'lang-chooser',
  'uses' => 'LangController@chooser'
));
