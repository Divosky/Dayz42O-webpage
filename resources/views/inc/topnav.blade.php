<nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse">
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#mainNav" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <a class="navbar-brand" href="#">Dayz<span class="accent-color">42O</span></a>

  <div class="collapse navbar-collapse" id="mainNav">
    <ul class="navbar-nav mr-auto mt-2 mt-md-0">
      <li class="nav-item">
        <a class="nav-item nav-link" href="index.php">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-item nav-link" href="changelog">Changelog</a>
      </li>
      <li class="nav-item">
        <a class="nav-item nav-link" href="maps">Maps</a>
      </li>
      <li class="nav-item">
        <a class="nav-item nav-link" href="items">Items</a>
      </li>
      <li class="nav-item">
        <a class="nav-item nav-link" href="faq">FAQ</a>
      </li>
      <li class="nav-item">
        <a class="nav-item nav-link" href="coins">Coins</a>
      </li>
      <li class="nav-item">
        <a class="nav-item nav-link" href="generator">Generator</a>
      </li>
      <li class="nav-item">
        <a class="nav-item nav-link" href="admin">ACP</a>
      </li>
      <li class="nav-item">
        <a class="nav-item nav-link" href="add-item">Add item</a>
      </li>
    </ul>
    <form action="{{ URL::route('lang-chooser') }}" method="post" class="form-inline">
      <select class="custom-select" name="locale" onchange="this.form.submit()">
        <option value="en">English</option>
        <option value="pl" {{ Lang::locale() === 'pl' ? 'selected' : ''}}>Polish</option>
        {{ Form::token() }}
      </select>
      <input type="submit" class="invisible" value="Send">
    </form>
  </div>
</nav>
