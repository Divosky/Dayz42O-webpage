<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Items Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for various usage messages
    |
    */

    'description' => 'Opis:',
    'engine' => 'Załóż toolbox i użyj engine.',
    'gascan' => 'Użyj go w pobliżu pojazdu.'

];
