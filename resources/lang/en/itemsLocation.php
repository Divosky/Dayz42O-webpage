<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Items Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for various usage messages
    |
    */

    'location' => 'Location:',
    'engine' => 'garage.',
    'gascan' => 'Użyj go w pobliżu pojazdu.'

];
