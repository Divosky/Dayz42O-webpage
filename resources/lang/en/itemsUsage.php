<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Items Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for various usage messages
    |
    */

    'usage' => 'Usage:',
    'engine' => 'Equip a toolbox and use the engine.',
    'gascan' => 'Use it near a vehicle.'

];
